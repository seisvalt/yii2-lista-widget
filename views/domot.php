<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 26/05/16
 * Time: 12:05 PM
 */

$buscador = <<<EOT
<div id="{$id}" class="cont-top-lateral">
    <div class="titulo">
        <div class="titulo-sensores">$title</div>
        <div class="filtros">
            <!-- <div>FILTROS:</div> -->
        </div>
    </div>
    <div class="cont-busqueda">
        <input id="input{$id}" class="input-buscar" type="text" name="" value="" >
        <i class="fa fa-search icono-buscar"></i>
    </div>
</div>
EOT;
$message ='<div id="list'.$id.'" class="contenedor-scroll">';

foreach ($data as $dato) {
    $nombre  	= $dato['name'];
    $id_nodo 	= $dato['id'];
    $direccion 	= $dato['address'];
    $estado = "sensor-ana";
    $message .=
<<<EOT
    <a class="item-sensor" href="#" id="{$id}" data-value="$id_nodo">
        <i class="fa fa-circle $estado estatus-item"></i>
        <div class="datos-item">
            <div class="nombre-sensor">$nombre</div>
            <div class="ubicacion-sensor"><i class="fa fa-map-marker"></i>
                <span>$direccion</span>
            </div>
        </div>
    </a>
EOT;

}
$message .='</div>';
$message =$buscador.$message;
echo $message;