<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 26/05/16
 * Time: 12:05 PM
 */

$buscador = <<<EOT
<div id="{$id}" class="cont-top-lateral">
    <div class="titulo">
        <div class="titulo-sensores">$title</div>
        <div class="filtros">
            <!-- <div>FILTROS:</div> -->
            <button name="estado" type="button" class="btn btn-list ALL active">TODOS</button>
            <button name="estado" type="button" class="btn btn-list OFF">OFFLINE</button>
            <button name="estado" type="button" class="btn btn-list ACO">ABIERTO</button>
            <button name="estado" type="button" class="btn btn-list ANA">ALERTA</button>
        </div>
    </div>
    <div class="cont-busqueda">
        <input id="input{$id}" class="input-buscar" type="text" name="" value="" >
        <i class="fa fa-search icono-buscar"></i>
    </div>
</div>
EOT;

// TODO IRIS-2  SCS Se debe agregar la posibilidad de usar el estado en  <i class="fa fa-circle sensor-online estatus-item"></i>
$message ='<div id="list'.$id.'" class="contenedor-scroll">';
foreach ($data as $dato) {
    $name  	= $dato['name'];
    $id_nodo 	= $dato['id'];
    $address 	= $dato['address'];
    $estado = "sensor-offline";

    if ($dato['funcional'] === true) {
        $estado = "sensor-online";
    }

    if ($dato['bl'] == 2) {
        $estado = "sensor-ana";
	}

    if ($dato['bl'] == 1) {
        $estado = "sensor-aco";
    }

    $message .= <<<EOT
    <a class="item-sensor" href="#" id="{$id}" data-value="$id_nodo">
        <i class="fa fa-circle $estado estatus-item"></i>
        <div class="datos-item">
            <div class="nombre-sensor">$name</div>
            <div class="ubicacion-sensor"><i class="fa fa-map-marker"></i>
                <span>$address</span>
            </div>
        </div>
    </a>
EOT;

}
$message .='</div>';
$message =$buscador.$message;
echo $message;
