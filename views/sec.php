<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 26/05/16
 * Time: 12:05 PM
 */

$buscador = <<<EOT
					<div id="{$id}" class="cont-top-lateral">
						<div class="titulo">
							<div class="titulo-sensores">$title</div>
							<div class="filtros">
								<!-- <div>FILTROS:</div> -->
								<button name="estado" type="button" class="btn btn-list TODOS active">TODOS</button>
								<button name="estado" type="button" class="btn btn-list ACO">ACO</button>
								<button name="estado" type="button" class="btn btn-list ANA">ANA</button>
							</div>
						</div>
						<div class="cont-busqueda">
							<input id="input{$id}" class="input-buscar" type="text" name="" value="" >
							<i class="fa fa-search icono-buscar"></i>
						</div>
					</div>
EOT;

// TODO IRIS-2  SCS Se debe agregar la posibilidad de usar el estado en  <i class="fa fa-circle sensor-online estatus-item"></i>

$message ='<div id="list'.$id.'" class="contenedor-scroll">';
//$estado = "sensor-aco";
foreach ($data as $dato) {
    $nombre  	= $dato['nombre'];
    $id_nodo 	= $dato['id'];
    $direccion 	= $dato['direccion'];
    $estado = ( $dato['bl'] == 1 )?"sensor-ana":"sensor-aco";
   //     $estado= $dato['bl'];
    $message .= <<<EOT
			<a class="item-sensor" href="#" id="{$id_nodo}" data-value="$id_nodo" onclick="ejecutarCentrado(${id_nodo})">
                    <i class="fa fa-circle $estado estatus-item"></i>
                    <div class="datos-item">
                        <div class="nombre-sensor">$nombre</div>
                        <div class="ubicacion-sensor"><i class="fa fa-map-marker"></i><span>$direccion</span></div>
                    </div>
            </a>
EOT;

}
$message .='</div>';
$message =$buscador.$message;
echo $message;
