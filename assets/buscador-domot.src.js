var estados= "TODOS";
var searchButton ;
var textToSearch;
var listado;
var options;

function ejecutarCentrado(id){
    centerLatLon(id);
    return false;
}


var buscadorLista =function(options){

    searchButton = $('#'+options.id+' .cont-busqueda i.icono-buscar');
    textToSearch = $('#input'+options.id);
    listado = $('#list'+options.id);
    var errorSpan = $('#'+options.id+' span.error');

    function cargarNodos(){
        var txt = jQuery.trim(textToSearch.val());
        console.info(options.url);

        $.ajax({
            method: "POST",
            dataType: "json",
            url: options.url,
            data: { searchtext: txt, estado: estados },
            success: function(result){
                listado.empty();

                for (i = 0; i < result.length; i++) {
                    estado = ( result[i].bl == 1 )?"sensor-ana":"sensor-aco";

                    var texto =
                        "<a class='item-sensor' href='#' id='"+options.id+"' " +
                        "data-value='"+result[i].id+"' " +
                        "onclick='ejecutarCentrado("+result[i].id+")'>" +
                        "<i class='fa fa-circle " + estado + " estatus-item'></i>"+
                        "<div class='datos-item'>"+
                            "<div class='nombre-sensor'>"+result[i].name+"</div>" +
                            "<div class='ubicacion-sensor'>"+
                                "<i class='fa fa-map-marker'></i>"+
                                "<span>"+result[i].address+"</span>" +
                            "</div>" +
                        "</div></a>";
                    listado.append(texto);
                }
            },
            error: function(result){
                console.log(result);

            }
        });

    }

    searchButton.click(function(){
        errorSpan.html('');
        if(txt.length > 0)
        {
            console.log(options.url);
            cargarNodos(estados);
        }
    });
    $('button[name=estado]').click(function () {
        $('button[name=estado]').removeClass('active');
        $(this).addClass('active');
        estados=$(this).text();
        cargarNodos();
    });
    $("#input"+options.id).focus();
    $("#input"+options.id).keyup(function(e){
        console.warn("tecla presionada");

        if ($("#input"+options.id).is(":focus") && (e.keyCode == 13)) {
            cargarNodos();
        }
    });
};