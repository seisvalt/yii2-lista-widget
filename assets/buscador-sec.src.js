var estados= "TODOS";
var searchButton ;
var textToSearch;
var listado;
var options;
var ct;

function ejecutarCentrado(id){
    console.log('Llamando funcion centerLatLon con parametro '+id);
    centerLatLon(id);
    return false;
}


var buscadorLista =function(options){

    searchButton = $('#'+options.id+' .cont-busqueda i.icono-buscar');
    textToSearch = $('#input'+options.id);
    listado = $('#list'+options.id);
    var errorSpan = $('#'+options.id+' span.error');

    function cargarNodos(){

        console.log(ct);
        var txt = jQuery.trim(textToSearch.val());
        console.info(options.url);
        $.ajax({
            method: "POST",
            dataType: "json",
            url: options.url,

            data: { searchtext: txt, estado: estados, ct: ct },
           // processData: false,
            success: function(result){
                listado.empty();
                for (i = 0; i < result.length; i++) {
                    estado = ( result[i].bl == 1 )?"sensor-ana":"sensor-aco";
                    var texto =
                        "<a class='item-sensor' href='#' id="+options.id+" " +
                        "data-value='"+result[i].id+"' onclick='ejecutarCentrado("+result[i].id+")'>\
                            <i class='fa fa-circle " + estado + " estatus-item'></i>\
                            <div class='datos-item'>\
                            <div class='nombre-sensor'>"+result[i].nombre+"</div>\
                            <div class='ubicacion-sensor'><i class='fa fa-map-marker'></i>\
                                <span>"+result[i].direccion+"</span></div>\
                            </div></a>";
                    listado.append(texto);
                }

                //console.log( "Sample of data:"+result);
                console.log( result);
            },
            error: function(result){
                console.warn("vacaaa");
                console.log(result);

            }
        });

    }

    searchButton.click(function(){
        errorSpan.html('');
        if(txt.length > 0)
        {
            console.log(options.url);
            cargarNodos(estados);

        }
    });

    $('#ct').on('select2:select', function (e) {
        var data = e.params.data;
        ct = data.id;
        cargarNodos();
    });

    $('button[name=estado]').click(function () {
        $('button[name=estado]').removeClass('active');
        $(this).addClass('active');
        estados=$(this).text();
        console.warn($(this).text());
        cargarNodos();
    });

    var consulta;
    console.info("opciones cargadas");
    console.log(options);

    //hacemos focus al campo de búsqueda
    $("#input"+options.id).focus();

    //comprobamos si se pulsa una tecla
    $("#input"+options.id).keyup(function(e){
        console.warn("tecla presionada");

        if ($("#input"+options.id).is(":focus") && (e.keyCode == 13)) {
            cargarNodos();
        }




    });

};
