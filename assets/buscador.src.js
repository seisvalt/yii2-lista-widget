var estados= "TODOS";
var searchButton ;
var textToSearch;
var listado;
var options;

function ejecutarCentrado(id){
    console.log('Llamando funcion centerLatLon con parametro '+id);
    centerLatLon(id);
    return false;
}


var buscadorLista =function(options){

    searchButton = $('#'+options.id+' .cont-busqueda i.icono-buscar');
    textToSearch = $('#input'+options.id);
    listado = $('#list'+options.id);
    //var loader = $('#'+options.id+' .searchbar img');
    //var resultsBar = $('#'+options.id+' .resultsbar');
    //var okButton = $('#'+options.id+' .resultsbar input.button');
    var errorSpan = $('#'+options.id+' span.error');

    function cargarNodos(){

        console.error(estados);
        var txt = jQuery.trim(textToSearch.val());
        console.error(txt);
        $.ajax({
            method: "POST",
            dataType: "json",
            url: options.url,

            data: { searchtext: txt, estado: estados },
           // processData: false,
            success: function(result){
                listado.empty();
                for (i = 0; i < result.length; i++) {
                    var texto =
                        "<a class='item-sensor' href='#' id="+options.id+" " +
                        "data-value='"+result[i].id+"' onclick='ejecutarCentrado("+result[i].id+")'>\
                            <i class='fa fa-circle sensor-online estatus-item'></i>\
                            <div class='datos-item'>\
                            <div class='nombre-sensor'>"+result[i].titulo+"</div>\
                            <div class='ubicacion-sensor'><i class='fa fa-map-marker'></i>\
                                <span>"+result[i].subtitulo+"</span></div>\
                            </div></a>";
                    listado.append(texto);
                }

                console.log( "Sample of data:"+result);
                console.log( result);
            },
            error: function(result){
                console.warn("vacaaa");
                console.log(result);

            }
        });

    }

    searchButton.click(function(){
     //   var txt = jQuery.trim(textToSearch.val()); // IE no soporta xxx.trim()
        errorSpan.html('');
        if(txt.length > 0)
        {
            //var url = options.url+"&searchtext="+txt;
            console.log(options.url);
            cargarNodos(estados);
            //selectList.find('option').each(function(){ $(this).remove(); });
            //loader.show();

/*
            $.getJSON(url, function(data) {
                var nitems=0;
                $.each(data, function(index, val) {
                    //selectList.append("<option value='"+index+"'>"+val+"</option>");
                    nitems++;
                });
                if(nitems == 0){
                    // no results found
                    //loader.hide();
                    resultsBar.hide('fast');
                    errorSpan.html('no results found');
                    setTimeout(function(){
                        errorSpan.html('');
                    },3000);
                    options.onEmptyResult(txt);
                }
                else{
                    //loader.hide();
                    resultsBar.show('fast');
                }
            }).error(function(e){
                //loader.hide();
                options.onError(e);
            });*/
        }
    });

    $('button[name=estado]').click(function () {
        $('button[name=estado]').removeClass('active');
        $(this).addClass('active');
        estados=$(this).text();
        console.warn($(this).text());
        cargarNodos();
    });

    var consulta;
    console.info("opciones cargadas");
    console.log(options);

    //hacemos focus al campo de búsqueda
    $("#input"+options.id).focus();

    //comprobamos si se pulsa una tecla
    $("#input"+options.id).keyup(function(e){
        console.warn("tecla presionada");

        if ($("#input"+options.id).is(":focus") && (e.keyCode == 13)) {
            cargarNodos();
        }




    });

};