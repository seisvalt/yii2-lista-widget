<?php
/**
 * Created by PhpStorm.
 * User: seisvalt
 * Date: 2/03/15
 * Time: 10:55 AM
 */
namespace seisvalt\listas;
use yii\base\Widget;
use yii\helpers\Html;
use Yii;
use yii\helpers\Json;
use yii\web\View;

class Lista_simple extends Widget{
	public $message;
	protected $constr = 'Chart';
	protected $baseScript = 'highcharts';
	private $id;
	public $options = [];
	public $data = [];
	public $model;
	public $htmlOptions = [];
	public $setupOptions = [];
	public $scripts = [];
	public $callback = false;
    private $title = "sensores";
	public $filter;
	public $searchModel;
	public $attributes;
	public $context;
	public $optionsJs;
    public $url;
	public $action=null;
	public $onSuccess;	// function(val,text){}
	public $onError;	// function(e){ alert(e.responseText); }
	public $onEmptyResult;	// function(txt){ }
	public $noResultsMessage='No results found.';


	public function init(){

		parent::init();
		if($this->message===null){
			$this->message= 'Se esperaba un dato';
		}
        if(is_array($this->options) && count($this->options) >= 1) {
            if (isset($this->options["title"]))
                $this->title = $this->options["title"];
        }
		$this->id = isset($this->htmlOptions["id"])?$this->htmlOptions["id"]:"list-".uniqid();
		if(is_array($this->data) && count($this->data) >= 1) {
			$view = \Yii::$app->params['typeMap'] ?? "index";
			$this->message= $this->render($view,
				[
					"data"=>$this->data,
					'id'=>$this->id,
					'title'=>$this->title,

				]);
			$modelAttributeId='';
			
			if(($this->model != null) && ($this->attribute != null))
				$modelAttributeId = get_class($this->model).'_'.$this->attribute;

			if($this->onSuccess == null)
				$this->onSuccess = 'function(val,text){ }';
				
			if($this->onError == null)
				$this->onError = 'function(e){ }';
				
			if($this->onEmptyResult == null)
				$this->onEmptyResult = 'function(txt){ }';

			$this->optionsJs = Json::encode(
				array(
					'id'=>$this->id,
                    'url'=>$this->url,
					'onSuccess'=>$this->onSuccess,
					'onError'=>$this->onError,
					'onEmptyResult'=>$this->onEmptyResult,
					'modelAttributeId'=>$modelAttributeId,
					'noResultsMessage'=>$this->noResultsMessage,
				)
			);
		}
		else{
			$this->message= 'No se ha enviado un recurso de datos';
		}
	}

	/**
	 * Renders the widget.
	 */
	public function run(){
		$this->registerAssets();

		return $this->message;
	}


	private function generarLista(){


		$buscador = <<<EOT
					<div id="{$this->id}" class="cont-top-lateral">
						<div class="titulo">
							<div class="titulo-sensores">$this->title</div>
							<div class="filtros">
								<!-- <div>FILTROS:</div> -->
								<button name="estado" type="button" class="btn btn-default active">TODOS</button>
								<button name="estado" type="button" class="btn btn-default">ONLINE</button>
								<button name="estado" type="button" class="btn btn-default">OFFLINE</button>
							</div>
						</div>
						<div class="cont-busqueda">
							<input id="input{$this->id}" class="input-buscar" type="text" name="" value="" >
							<i class="fa fa-search icono-buscar"></i>
						</div>
					</div>
EOT;

		// TODO IRIS-2  SCS Se debe agregar la posibilidad de usar el estado en  <i class="fa fa-circle sensor-online estatus-item"></i>

		$this->message ='<div id="list'.$this->id.'" class="contenedor-scroll">';
		$estado = "sensor-online";
		foreach ($this->data as $dato) {
			$nombre  	= $dato['nombre'];
			$id_nodo 	= $dato['id'];
			$direccion 	= $dato['direccion'];
			if(isset($dato['estado_css']))
				$estado= $dato['estado_css'];
			$this->message .= <<<EOT
			<a class="item-sensor" href="#" id="$id_nodo" data-value="$id_nodo">
                    <i class="fa fa-circle $estado estatus-item"></i>
                    <div class="datos-item">
                        <div class="nombre-sensor">$nombre</div>
                        <div class="ubicacion-sensor"><i class="fa fa-map-marker"></i><span>$direccion</span></div>
                    </div>
            </a>
EOT;

		}
		$this->message .='</div>';
		$this->message =$buscador.$this->message;
	}

	/**
	 * Registers required assets and the executing code block with the view
	 */
	protected function registerAssets()
	{
		$js = "
		new buscadorLista({$this->optionsJs});
		var nodo_select=0;
				$('a[id=$this->id]').click(
					function(){
						console.log('Llamando funcion centerLatLon con parametro '+$(this).attr('data-value'));
						centerLatLon($(this).attr('data-value'));
						return false;
						}
				);";
		$key = __CLASS__ . '#' . $this->id;
		$this->view->registerJs($js, View::POS_LOAD, $key);
		ListAsset::register($this->view);
	}
}
?>
